package com.yeyh.dubboserver.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.yeyh.baseapi.service.IHelloService;
import org.springframework.stereotype.Component;

/**
 * @author yeyh
 */
@Component
@Service(interfaceClass = IHelloService.class)
public class HelloServiceImpl implements IHelloService {
    @Override
    public void sayHello() {
        System.out.println("Hello World");
    }
}
