package com.yeyh.dubboclient;

import com.alibaba.dubbo.spring.boot.annotation.EnableDubboConfiguration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DubboClientApplicationTests {

	@Test
	public void contextLoads() {
	}

}
