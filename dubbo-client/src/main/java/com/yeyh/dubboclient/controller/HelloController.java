package com.yeyh.dubboclient.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.yeyh.baseapi.service.IHelloService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author yeyh
 */
@RestController
public class HelloController {

    @Reference(url = "dubbo://127.0.0.1:20880")
    private IHelloService iHelloService;

    @RequestMapping("/hello")
    public void sayHello() {
        iHelloService.sayHello();
    }

}
